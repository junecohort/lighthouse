#! /usr/bin/env ruby
# This is the main entrypoint into the program
# It requires the other files/gems that it needs

require 'pry'
require './candidates'
require './filters'
require 'colorize'

## Your test code can go here

# binding.pry

# pp "The candidate is experienced(true): " << experienced?(@candidates[2]).to_s #=> true
# pp "The candidate is experienced(false): " << experienced?(@candidates[1]).to_s #=> false
#
# pp "Finding candidate id: 5  \nvalue: " << find(5).to_s
# pp "Finding candidate id: 0 (nil)\nvalue: " << find(0).to_s
#
# pp "Knows correct language(true): \nvalue: " << knows_correct_language?(@candidates[0]).to_s #=> true
# pp "Knows correct language(false): \nvalue: " << knows_correct_language?(@candidates[2]).to_s #=> false
#
# pp "Applied within correct range(true): \nvalue: " << applied_within_range?(@candidates[0]).to_s #=> true
# pp "Applied within correct range(false): \nvalue: " << applied_within_range?(@candidates[1]).to_s #=> false
#
# pp "Git points are over 100(true): \nvalue: " << git_over_100?(@candidates[0]).to_s
#
# pp "Candidate is over 17(true): \nvalue: " << over_17?(@candidates[0]).to_s
# pp "Candidate is over 17(false): \nvalue: " << over_17?(@candidates[6]).to_s

# pp qualified_candidates @candidates

# print "Ordered by experience || git: \nvalue: " << ordered_by_qualifications(@candidates).to_s



loop do
  puts "Quit - Exit program\nAll - list out all candidates\nQualified - List qualified candidates\nFind id - Find candidate with that id"
  print "Enter a command: "
  input = gets.chomp.downcase

  case input

  when 'quit'
    break

  when 'qualified'
    pp ordered_by_qualifications qualified_candidates(@candidates)

  when 'all'
    @candidates.each do |cand|
      puts @qualified.call(cand) ? cand.to_s.green : cand.to_s.red
    end

  when /^find/
    pp find(input.split.last.to_i)
  else
    puts "Your input is invalid"
  end
  print "Press any key to continue..."
  STDIN.getc
  system "clear" or system "cls"
end
