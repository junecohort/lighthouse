# In this file we define the methods to help filter out candidates
# This way, we keep these methods separated from other potential parts of the program

def find id
  @candidates.each do |cand|
    return cand if cand[:id] == id
  end
  nil
end

def knows_correct_language? candidate
  languages = candidate[:languages]

  "Ruby".in?(languages) || "Python".in?(languages)
end

def applied_within_range? candidate
  candidate[:date_applied] > 15.days.ago.to_date
end

def git_over_100? candidate
  candidate[:github_points] >= 100
end

def over_17? candidate
  candidate[:age] > 17
end

def experienced? candidate
  candidate[:years_of_experience] > 1
end


@qualified = lambda {|cand|   #Ignore this and :qualified_candidates method use your own method of telling qualified
  knows_correct_language?(cand) &&
  applied_within_range?(cand) &&
  git_over_100?(cand) &&
  over_17?(cand) &&
  experienced?(cand)
}

# More methods will go below
define_method :qualified_candidates do |candidates|
  candidates.select(&@qualified)
end

def ordered_by_qualifications candidates
  candidates.sort do |cand1, cand2|
    if cand1[:years_of_experience] == cand2[:years_of_experience]
      cand2[:github_points] <=> cand1[:github_points]
    else
       cand2[:years_of_experience] <=> cand1[:years_of_experience]
    end
  end
end
