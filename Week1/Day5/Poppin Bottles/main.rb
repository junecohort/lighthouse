#!/usr/bin/env ruby

require './bottles.rb'



puts "Test Bottle Return Algorithm"
puts "****************************"
puts "Expect call with investment (0) Expect: 0\nActual: #{calculate_bottles(0).to_s}\n"
puts "Expect call with investment (2) Expect: 1\nActual: #{calculate_bottles(2).to_s}\n"
puts "Expect call with investment (8) Expect: 7\nActual: #{calculate_bottles(8).to_s}\n"
puts "Expect call with investment (300) Expect: 262\nActual: #{calculate_bottles(300).to_s}\n"
