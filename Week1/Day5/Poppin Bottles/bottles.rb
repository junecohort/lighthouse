
def calculate_bottles investment
  total_bottles = 0
  if investment <= 0
    total_bottles
  else
    until investment <= 0 do
      bottles_created = investment / 2

      caps = bottles_created / 4
      bottles_from_bottles = bottles_created / 2

      new_bottles = caps + bottles_from_bottles 



      total_bottles += bottles_created + new_bottles

      investment = new_bottles * 2
    end
    # calculate_bottles investment
  end
  total_bottles
end
