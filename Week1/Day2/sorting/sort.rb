#!/usr/bin/env ruby

# Sort the array from lowest to highest
# def sort(arr) #bubble sort
#   current_counter, checking_counter = 0
#   until current_counter == arr.length do
#     until checking_counter == arr.length do
#       # puts checking_counter
#       if arr[current_counter] > arr[checking_counter]
#         arr[current_counter], arr[checking_counter] = arr[checking_counter], arr[current_counter]
#       else
#         checking_counter += 1
#       end
#     end
#     current_counter += 1
#     checking_counter = 0
#   end
#   arr.reverse
# end


def sort(arr) #merge sort
  result = []
  current_counter = 0
  checking_counter = 0

  arr << 0  if arr.length % 2 != 0
  arr = arr.each_slice(1).to_a

  
end

# Find the maximum
def maximum(arr)
  sort(arr).last
end

def minimum(arr)
  sort(arr).first
end

# expect it to return 42 below
result = []
time = Time.now
100000.times { result.push(Random.rand(500)) }
puts "max of 2, 42, 22, 02 is: #{maximum(result)}"
puts Time.now - time

# expect it to return 2 below
result = minimum([2, 42, 22, 02])
puts "min of 2, 42, 22, 02 is: #{result}"


# expect it to return nil when empty array is passed in
result = maximum([])
puts "max on empty set is: #{result.inspect}"
result = minimum([])
puts "min on empty set is: #{result.inspect}"

result = maximum([-23, 0, -3])
puts "max of -23, 0, -3 is: #{result}"

result = maximum([6])
puts "max of just 6 is: #{result}"
