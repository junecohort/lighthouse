def div_5? x
  x %   5 == 0
end

def div_3? x
  x % 3 == 0
end

def fizz_buzz y
  if div_3?(y) && div_5?(y)
    "FizzBuzz"
  elsif div_5? y
    "Buzz"
  elsif div_3? y
    "Fizz"
  else
    y
  end
end

def fbs s, f
  s.upto(f) do |x|
    puts fizz_buzz x
  end
end

a = 60
b = 80
fbs(a, b)
