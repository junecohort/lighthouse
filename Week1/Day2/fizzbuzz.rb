module MyFixnum
  def divisable_by_5?
    self % 5 == 0
  end

  def divisable_by_3?
    self % 3 == 0
  end
end

class Fixnum
  include MyFixnum
end


def fizz_buzz x, y
  (x..y).each do |i|
    if i.divisable_by_5? && i.divisable_by_3?
      puts "FizzBuzz"
    elsif i.divisable_by_5?
      puts "Buzz"
    elsif i.divisable_by_3?
      puts "Fizz"
    else
      puts i
    end
  end
end

fizz_buzz 1, 100
