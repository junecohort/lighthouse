def sign height, width, color = []
  total = 0
  sqrfeet = height * width
  total = sqrfeet * 15

  print "Enter your colors seperated with commas: "
  color = gets.chomp.split(',').trim

  if color.length > 2
    total += color.length * 15
  else
    total += color.length * 10
  end

  total += total * 0.15

end

puts "Expect sign(4,4) equal: 276 actual: " << sign(4,4).to_s

puts "Expect sign(4,4,['blue','green','red']) equal: 327.75 actual: " << sign(4,4,['blue','green','red']).to_s
