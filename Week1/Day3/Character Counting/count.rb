def count_letters string
  result = {}
  string.downcase.chars.each do |letter|
    next if letter == " "
    result[letter] = 0 if result.has_key? letter
    result[letter] += 1
  end
  result
end

def index_letters string
  result = {}
  string.chars.each_with_index do |item, index|
    if result.has_key? item
      result[item] << index
    else
      result[item] = [index]
    end
  end
  result
end

puts count_letters 'hello thereeeee'

puts index_letters 'yellow is a color'
