#!/usr/bin/env ruby
def method1
  method2
end

def method2
  method3 'Hello I was a bug'
end

def method3(a)
  puts a
end

method1
