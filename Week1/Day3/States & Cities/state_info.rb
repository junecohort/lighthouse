#!/usr/bin/env ruby
states = {
  CA: ['California', ['Alameda', 'Apple Valley', 'Exeter']],
  FL: ['Florida', ['Amelia Island', 'Bunnell']],
  MI: ['Michigan', ['Ann Arbor', 'East China', 'Elberta']],
  NY: ['New York', ['Angelica', 'Apalachin', 'Canadice']],
  OR: ['Oregon', ['Amity', 'Boring', 'Camas Valley']],
}

states[:CO] = ['Colorado', ['Blanca', 'Crestone', 'Dillon', 'Fairplay']]
states[:HI] = ['Hawaii', ['Kailua', 'Hoopili', 'Honolulu']]


taxes = {
  CA: 1.08,
  CO: 1.035,
  FL: 1.065,
  HI: 1.075,
  MI: 1.01,
  NY: 1.011,
  OR: 1.012,
}

module MyString
  def capitalize_each
    self.split.map(&:capitalize).join(' ')
  end
end

class String
  include MyString
end

define_method :describe_state do |state|
  state = state.to_sym
  description = "#{state.to_s} is for #{states[state].first}."
  description << " It has #{states[state].last.length} major cities:"
  states[state].last.each do |x|
    description << " #{x}"
    if x == states[state].last.last
      description << '.'
    else
      description << ','
    end
  end
  description
end

define_method :calculate_tax do |state, dollars|
  return nil unless states.has_key? state.to_sym
  sprintf '%.2f', dollars * taxes[state.to_sym].to_f
end

define_method :find_state_for_city do |city|
  result = ''
  states.values.each do |state, value|
    if value.include? city.to_s.gsub('_', ' ').capitalize_each
      result = state
    end
  end
  result
end




puts describe_state :CA
puts describe_state 'FL'
puts describe_state 'NY'

puts calculate_tax :FL, 100
puts calculate_tax 'FL', 100
puts calculate_tax 'NA', 100
puts calculate_tax 'NY', 300

puts find_state_for_city :apple_valley
puts find_state_for_city 'Apple Valley'
puts find_state_for_city 'East China'
