#! /usr/bin/env ruby
# require 'pry'
numerals = {
  1000 => "M",
  900 => "CM",
  500 => "D",
  400 => "CD",
  100 => "C",
  90 => "XC",
  50 => "L",
  40 => "XL",
  10 => "X",
  9 => "IX",
  5 => "V",
  4 => "IV",
  1 => "I"
}

# (result, num, x)
# define_method :to_roman do |num|
#   result ||= ''
#   mod_result ||= 0
#   remainder ||= 1000
#   number = num
#   numerals.keys.each do |x|
#     mod_result, remainder = number.divmod(x)
#     result << numerals[x] * mod_result
#     number = remainder
#   end
#   result
# end

def to_roman num

end


define_method :to_roman do |num|
  result = ''
  mod_result = 0
  remainder = 0
  number = num
  numerals.keys.each do |x|
    # binding.pry
    mod_result, remainder = number.divmod(x)
    result << numerals[x] * mod_result
    number = remainder
  end
  result
end

puts to_roman(2)
puts to_roman(4000)
puts to_roman(23423)
# Drive code... this should print out trues.
puts to_roman(1) == "I"
puts to_roman(3) == "III"
puts to_roman(6) == "VI"

puts "My totally sweet testing script\n"
puts "input | expected | actual"
puts "------|----------|-------"
puts "4     | IV       | #{to_roman(4	)}"
puts "9     | IX       | #{to_roman(9)}"
puts "13    | XIII     | #{to_roman(13)}"
puts "1453  | MCDLIII  | #{to_roman(1453)}"
puts "1646  | MDCXLVI  | #{to_roman(1646)}"
