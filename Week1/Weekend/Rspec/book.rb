class Book
  attr_accessor :title, :author, :category

  def initialize title, author, category
    self.title = title
    self.author = author
    self.category = category
  end
end
