class Barracks
  attr_reader :food, :gold

  def initialize
    @food = 80
    @gold = 1000
  end

  def train_footman
    if can_train_footman?
      @gold -= 135
      @food -= 2
      Footman.new
    end
  end

  def train_peasant
    if can_train_peasant?
      @food -= 5
      @gold -= 90
      Peasant.new
    end
  end

  def can_train_footman?
    self.food >= 2 && self.gold >= 135
  end

  def can_train_peasant?
    self.food >= 5 && self.gold >= 90
  end
end
