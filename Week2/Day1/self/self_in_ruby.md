`self` is this mysterious keyword in Ruby. You can use this keyword in order to access the context that the program is currently working inside of. There are three main contexts to consider, though you will likely only ever use self in two of them.

There is the global or "main" context, which you can see by executing `self.to_s` or `self.public_methods` in IRB or Pry.

**Consider this code:**

    class Person
      def self.example_class_method
        puts "We're calling an example class method"
        puts "'self' is always defined.  What is 'self' here?  Let's see."
        p self
        puts "That was self!"
      end
    
      def example_instance_method
        puts "We're calling an example *instance* method"
        puts "'self' is defined here, too, but it means something different."
        p self
        puts "That was self, again, but see how it's an instance of the class."
      end
    end

We've defined two methods, here:  

1. a class method 
2. an instance method

`self` references something different depending on the context. In the class method context, `self` refers to the `class` itself, which is like running `Person` in IRB. 

In the instance method context, `self` refers to the particular instance of the class.

Try this code:

    Person.example_class_method
    person = Person.new
    person.example_instance_method

There is the class or module context, which you can see here:

    class Person
      puts "You'll see this as the class is being defined."
      puts "In this context, self is: "
      p self
      puts "See? self is the Person class."
    end

In practice, you'll care about `self` in 2 situations. First, when you want to create "class methods", like `Math.hypot(3, 4)`. 

### Class Methods

The simplest way to define a class method is like this (try it out):

    module Math
      def self.hypot(a, b)
        # maths in here
      end
    end
    
This is identical to referring to the class explicitly:
    
    module Math
      def Math.hypot(a, b)
        # maths in here
      end
    end

In this context, `self` is the class or module, and most Rubyists use `self` to define class methods.

## Resources

Read the following resource for more detailed explanation on `self`. 

1. <http://www.jimmycuadra.com/posts/self-in-ruby>

### Advanced Readings (Optional)

1. <http://openmymind.net/2010/6/25/Learning-Ruby-class-self/>. This one actually teaches another approach (`class << self`) to defining class methods on a class
2. <http://yehudakatz.com/2009/11/15/metaprogramming-in-ruby-its-all-about-the-self/>. This one gets into Meta-programming with Ruby. 

## Objectives

1. Read the above explanation of `self`. 
2. Read the external resource linked to for more detailed explanations / examples.
3. Write up an explanation for it in your own words, in your own gist.
