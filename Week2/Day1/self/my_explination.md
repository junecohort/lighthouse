Self Keyword
===

Self refers to the current object in which it is called. using self in an method name makes the method static

when used on a method it creates a instance method (Static Method)

when used like

    class << self
      def method

      end
      self.name #wouldnt work
    end

does the same



    c = Person.new

    def c.speak
      #meta-programming (only works on that instanceo f the class)
    end
