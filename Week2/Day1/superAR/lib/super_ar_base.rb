module SuperAR
	class Base
		def self.filename
			@filename
		end

		def self.database=(filename)
			@filename = filename.to_s
			@connection = SQLite3::Database.new(filename.to_s)

      @connection.results_as_hash  = true

      @connection.type_translation = true
		end

		def self.execute(sql)
			@connection.execute sql
		end

	  def set_attributes
	  	SuperAR::Base.execute("PRAGMA table_info(#{self.class.to_s.downcase + 's'})").each do |row|
	  		self.class.send(:define_method, "#{row["name"]}=") do |value|
	  			instance_variable_set("@#{row["name"]}", value)
	  		end

	  		self.class.send(:define_method, "#{row["name"]}") do
	  			instance_variable_get("@#{row["name"]}")
	  		end

	  	end
	  end

		def self.find id
			row = SuperAR::Base.execute "SELECT * FROM table_info(#{self.class.to_s.downcase + 's'}) WHERE Id=#{id}"
			puts row.join "\s"
		end

		def self.all
			SuperAR::Base.execute("select * from table_info(#{self.class.to_s.downcase + 's'})")
		end

		#save self into database?
		def save
			SuperAR::Base.execute("insert into jobs (name, location, email, phone, created_at, updated_at)
													   values ('#{self.name}', '#{self.location}', '#{self.email}', '#{self.phone}', '#{Time.now}', '#{Time.now}')")
		end

		def validate_phone number
			!!(number =~ /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i)
		end

		def validate_email email
			!!(email =~ /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)
		end

		def validate_location location
			# !!(location =~ )
		end

		def validate_name name
			# !!(name =~ /\b[a-zA-Z]+\b{1,2}/)
		end


	  def initialize
	  	set_attributes
		end
	end
end
