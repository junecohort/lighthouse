require 'pry'
require_relative './exception.rb'
class Robot
  attr_reader :position, :items, :shield
  attr_accessor :equipped_weapon, :health

  MAX_HEALTH = 100
  MAX_WEIGHT = 250
  DEFAULT_DAMAGE = 5
  MAX_RANGE = 1

  class << self
    attr_reader :robots

    def in_position(x, y)
      @robots.select do |robot|
        robot.position[0] == x && robot.position[1] == y
      end
    end
  end

  @robots = []

  def initialize
    @position = [0, 0]
    @items = []
    @health = MAX_HEALTH
    @shield = 50
    self.class.robots << self
  end

  def move_left
    @position[0] -= 1
  end

  def move_right
    @position[0] += 1
  end

  def move_up
    @position[1] += 1
  end

  def move_down
    @position[1] -= 1
  end

  def pick_up(item)
    if can_carry_item? item
      process_item item
      return true
    end
    false
  end

  def can_carry_item?(item)
    weight_with_item = items_weight + item.weight
    weight_with_item <= MAX_WEIGHT
  end

  def process_item(item)
    if item.class == BoxOfBolts && (@health <= 80)
      item.feed(self)
    elsif item.class < Weapon
      @equipped_weapon = item
    else
      @items << item
    end
  end

  def items_weight
    weight = 0
    @items.each do |item|
      weight += item.weight
    end
    weight
  end

  def wound(amount)
    if @shield > 0
      shield_damage amount
    else
      health_damage amount
    end
  end

  def scan
    robots_in_proximity = []
    x = @position[0]
    y = @position[1]
    for i in (x-1)..(x+1)
      robots_in_proximity << Robot.in_position(i, y)
    end
    for i in (y-1)..(y+1)
      robots_in_proximity << Robot.in_position(x, i)
    end
    robots_in_proximity.flatten.uniq.delete_if { |robot| robot == self}
  end

  def health_damage(amount)
    new_health = @health - amount
    if new_health > 0
      @health = new_health
    else
      @health = 0
    end
  end

  def shield_damage(amount)
    shield_health = @shield - amount
    if shield_health < 0
      @shield = 0
      @health += shield_health
    else
      @shield = shield_health
    end
  end

  def heal(amount)
    new_health = @health + amount
    if new_health > MAX_HEALTH
      @health = MAX_HEALTH
    else
      @health = new_health
    end
  end

  def heal!(amount)
    new_health = @health + amount
    raise RobotAlreadyDeadError if dead?
    heal(amount)
  end

  def dead?
    @health <= 0
  end

  def attack(enemy)
    if !out_of_range? enemy
      if @equipped_weapon
        @equipped_weapon.hit(enemy)
      else
        enemy.wound DEFAULT_DAMAGE
      end
    elsif have_grenades?
      equip_grenade unless is_grenade? @equipped_weapon
      if in_weapon_range? @equipped_weapon, enemy
        @equipped_weapon.hit enemy
        @equipped_weapon = nil
      end
    end
  end

  def in_weapon_range?(weapon, enemy)
    difference_x = (enemy.position[0] - @position[0]).abs
    difference_y = (enemy.position[1] - @position[1]).abs
    max_weapon_range = weapon.range
    difference_x <= max_weapon_range && difference_y <= max_weapon_range
  end

  def have_grenades?
    return true if is_grenade? @equipped_weapon
    @items.each do |item|
      return true if is_grenade? item
    end
    false
  end

  def is_grenade?(item)
    item.class == Grenade
  end

  def equip_grenade
    grenade = @items.detect { |item| is_grenade? item }
    # binding.pry
    @equipped_weapon = grenade
    @items.delete(grenade)
  end

  def out_of_range?(enemy)
    difference_x = (enemy.position[0] - @position[0]).abs
    difference_y = (enemy.position[1] - @position[1]).abs
    # binding.pry
    difference_y > MAX_RANGE || difference_x > MAX_RANGE
  end

  def charge_shield(amount)
    @shield += amount
  end

  def attack!(enemy)
    raise UnattackableEnemy unless enemy.class == Robot
    attack(enemy)
  end
end
