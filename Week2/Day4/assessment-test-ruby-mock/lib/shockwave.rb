class Shockwave < Weapon
  def initialize
    super "Shockwave", 50, 30
  end

  def hit(robot)
    enemies = robot.scan
    enemies.map do |enemy_robot|
      enemy_robot.health_damage @damage
      binding.pry
    end
    enemies
  end
end
