class Battery < Item
  attr_reader :shield_returned
  MAX_RECHARGE = 50

  def initialize
    super "Battery", 5
    @shield_returned = MAX_RECHARGE
  end

  def charge(robot)
    if robot.shield < MAX_RECHARGE
      heal_amount = MAX_RECHARGE - robot.shield
      robot.charge_shield heal_amount
      return true
    end
    false
  end
end
