require './spec_helper'

describe Robot do
  describe ".robots" do
    before :each do
      Robot.robots.clear
    end
    
    after :each do
      Robot.robots.clear
    end
    it 'starts at 0' do
      expect(Robot.robots.size).to eq 0
    end

    it 'adds robots to array' do
      robot = Robot.new
      expect(Robot.robots.size).to eq 1
    end

    it 'stores multiple robots' do
      robot = Robot.new
      robot = Robot.new
      expect(Robot.robots.size).to eq 2
    end
  end
end
