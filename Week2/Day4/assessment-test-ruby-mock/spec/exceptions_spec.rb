require './spec_helper'

describe Robot do
  subject(:robot) { Robot.new }
  describe '#heal!' do
    it 'heals if unit is not dead' do
      robot.wound(145)
      robot.heal!(30)
      expect(robot.health).to eq 35
    end

    it 'raises error if unit is dead' do
      robot.wound(150)
      expect(lambda{ robot.heal!(3) }).to raise_error RobotAlreadyDeadError
    end
  end

  describe '#attack!' do
    it 'should attack other robots' do
      robot2 = Robot.new
      robot2.wound(50)
      robot.attack! robot2
      expect(robot2.health).to eq 95
    end

    it 'should raise error if is Item' do
      item = Item.new 'Test', 9001
      expect(lambda {robot.attack! item}).to raise_error UnattackableEnemy
    end
  end
end
