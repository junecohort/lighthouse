require './spec_helper'

describe Robot do
  subject(:robot) { Robot.new }

  describe '#scan' do
    before :each do
      Robot.robots.clear
    end

    after :each do
      Robot.robots.clear
    end
    it 'returns robot on same spot' do
      robot2 = Robot.new
      expect(robot.scan).to eq [robot2]
    end

    it 'returns robot 1 block above' do
      robot2 = Robot.new
      robot2.move_up
      expect(robot.scan).to eq [robot2]
    end

    it 'returns robot one block below' do
      robot2 = Robot.new
      robot2.move_down
      expect(robot.scan).to eq [robot2]
    end

    it 'returns robot one block left' do
      robot2 = Robot.new
      robot2.move_left
      expect(robot.scan).to eq [robot2]
    end

    it 'returns robot one block right' do
      robot2 = Robot.new
      robot2.move_right
      expect(robot.scan).to eq [robot2]
    end

    it 'returns multiple robots around' do
      robot2 = Robot.new
      robot3 = Robot.new
      robot4 = Robot.new
      robot2.move_left
      robot3.move_right
      robot4.move_down
      expect(robot.scan).to eq [robot2, robot3, robot4]
    end
  end
end
