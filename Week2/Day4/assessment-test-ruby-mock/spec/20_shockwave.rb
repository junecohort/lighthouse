require './spec_helper'

describe Shockwave do
  subject(:shockwave) { Shockwave.new }
  it 'inherits from Weapon' do
    expect(shockwave.class.superclass).to eq Weapon
  end

  it 'has 30 damage' do
    expect(shockwave.damage).to eq 30
  end

  describe '#hit' do
    before :each do
      Robot.robots.clear
    end

    after :each do
      Robot.robots.clear
    end
    let(:robot) { Robot.new }
    let(:robot2) { Robot.new }
    it 'does 30 damage always to hp(ignore shield)' do
      shockwave.hit robot
      expect(robot2.health).to eq 70
    end

    it 'damages all targets around' do
      robot3 = Robot.new
      shockwave.hit robot
      expect(robot3.health).to eq 70
      expect(robot2.health).to eq 70
    end
  end
end
