require './spec_helper'
describe '#shield' do
  subject(:robot) { Robot.new }

  it 'starts with 50 shield' do
    expect(robot.shield).to eq 50
  end

  it 'subtracts damage from shield if possible' do
    robot.wound(20)
    expect(robot.shield).to eq 30
  end

  it 'subtracts damage from health if shield is 0' do
    robot.wound(50)
    robot.wound(20)
    expect(robot.health).to eq 80
  end

  it 'subtracts damage from health if damage is to high for shield' do
    robot.wound(60)
    expect(robot.health).to eq 90
  end

  it 'sets shield to zero if damage is greater' do
    robot.wound(60)
    expect(robot.health).to eq 90
    expect(robot.shield).to eq 0
  end
end
