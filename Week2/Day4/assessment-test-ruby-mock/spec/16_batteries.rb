require './spec_helper'

describe Battery do
  subject(:battery) { Battery.new }
  describe '#initialize' do
    it 'creates instance of class' do
      expect(battery).to be_a Battery
    end

    it 'sets battery shield return to 50' do
      expect(battery.shield_returned).to eq 50
    end

    it 'sets weight to 5' do
      expect(battery.weight).to eq 5
    end
  end

  describe '#charge' do
    let(:robot) { Robot.new }
    it 'charges robot shield to max 50' do
      robot.wound 20
      battery.charge robot
      expect(robot.shield).to eq 50
    end

    it 'does not charge shield if full' do
      expect(battery.charge robot).to eq false
    end
  end
end

describe Robot do
  describe '#charge_shield' do
    subject(:robot) { Robot.new }
    let(:battery) { Battery.new }
    it 'recharges shield not changing health' do
      robot.wound(60)
      battery.charge robot
      expect(robot.shield).to eq 50
      expect(robot.health).to eq 90
    end
  end
end
