require './spec_helper'

describe Robot do
  describe '#in_position' do

    it 'returns robot for 0,0' do
      robot = Robot.new
      expect(Robot.in_position 0, 0).to eq [robot]
    end

    it 'returns proper cords for 1 up 1 right' do
      robot = Robot.new
      robot.move_up
      robot.move_right
      expect(Robot.in_position 1, 1).to eq [robot]
    end

    it 'returns proper cords for 1 down 1 left' do
      robot = Robot.new
      robot.move_down
      robot.move_left
      expect(Robot.in_position -1, -1).to eq [robot]
    end
  end
end
