class Barracks
  attr_reader :food, :gold, :health, :lumber

  def initialize
    @food = 80
    @gold = 1000
    @health = 400
    @lumber = 500
  end

  def can_train_footman?
    self.food >= 2 && self.gold >= 135
  end

  def can_train_peasant?
    self.food >= 10 && self.gold >= 90
  end

  def can_train_seige_engine?
    self.gold >= 200 &&
    self.lumber >= 60 &&
    self.food >= 3
  end

  def can_upgrade?
    self.gold >= 200 && self.food >= 5
  end


  def dead?
    self.health <= 0
  end

  def upgrade
    if can_upgrade?
      @health += 200
      @gold = gold - 200
      @food -= 5
    else
      false
    end
  end

  def damage amount
    @health -= amount
  end

  def train_seige_engine
    if can_train_seige_engine?
      @gold -= 200
      @lumber -= 60
      @food -= 3
      SeigeEngine.new
    end
  end

  def train_footman
    if can_train_footman?
      @gold -= 135
      @food -= 2
      Footman.new
    else
      nil
    end
  end

  def train_peasant
    if can_train_peasant?
      @food -= 5
      @gold -= 90
      Peasant.new
    else
      nil
    end
  end
end
