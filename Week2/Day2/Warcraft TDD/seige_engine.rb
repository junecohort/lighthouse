class SeigeEngine < Unit
  def initialize
    super 400, 50
  end

  def attack! enemy
    if enemy.class == Barracks
      enemy.damage (attack_power * 2)
    elsif enemy.class == SeigeEngine
      enemy.damage (attack_power)
    end
  end
end
