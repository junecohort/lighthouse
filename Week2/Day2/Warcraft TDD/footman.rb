# http://classic.battle.net/war3/human/units/footman.shtml
class Footman < Unit
  def initialize
    super 60, 10
  
  def attack! enemy
    if enemy.dead? || dead?
      return nil
    elsif enemy.class == Barracks
      enemy.damage (self.attack_power / 2).ceil
    else
      enemy.damage self.attack_power


    end
  end
end
