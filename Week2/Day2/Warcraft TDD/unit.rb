class Unit
  attr_reader :health, :attack_power

  def initialize hp, ap
    @health = hp
    @attack_power = ap
  end

  def attack! enemy
    if (dead? || enemy.dead?)
      return nil
    else
      enemy.damage self.attack_power
    end
  end

  def damage amount
    @health -= amount
  end

  def dead?
    self.health <= 0
  end
end
