require 'spec_helper.rb'

describe SeigeEngine do
  subject(:seige) { SeigeEngine.new }

  it "starts with 400 HP" do
    expect(seige.health).to eq 400
  end

  it "starts with 50 AP" do
    expect(seige.attack_power).to eq 50
  end

  describe "#attack" do
    it "won't attack units" do
      unit = Unit.new(30, 40)
      # expect(unit).to receive(:damage).with(0)
      expect(seige.attack! unit).to be_nil
    end

    it "won't attack footmen" do
      footman = Footman.new
      expect(seige.attack! footman).to be_nil

    end

    it "can attack other seige engines" do
      seige2 = SeigeEngine.new
      expect(seige2).to receive(:damage).with(50)
      seige.attack! seige2
    end

    it "does double damage to barracks" do
      barracks = Barracks.new
      expect(barracks).to receive(:damage).with(100)
      seige.attack! barracks
    end
  end
end

describe Barracks do
  subject(:barrack){ Barracks.new }

  describe "#can_train_seige_engine?" do
    it "returns false if doesn't have enough gold" do
      expect(barrack).to receive(:gold).and_return(1)
      expect(barrack.can_train_seige_engine?).to eq false
    end

    it "returns false if doesn't have enough food" do
      expect(barrack).to receive(:food).and_return(1)
      expect(barrack.can_train_seige_engine?).to eq false
    end

    it "returns false if doesn't have enough lumber" do
      expect(barrack).to receive(:lumber).and_return(1)
      expect(barrack.can_train_seige_engine?).to eq false
    end

    it "returns true if have enough resources" do
      expect(barrack.can_train_seige_engine?).to eq true
    end
  end

  describe "#train_seige_engine" do
    it "creates new seige engine if has resources" do
      seige_engine = barrack.train_seige_engine
      expect(seige_engine).to be_an_instance_of(SeigeEngine)
    end

    it "doesn't create if doesn't have resources" do
      expect(barrack).to receive(:lumber).and_return(1)
      expect(barrack.train_seige_engine).to be_nil
    end
  end
end
