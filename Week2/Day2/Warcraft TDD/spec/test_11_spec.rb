require 'spec_helper'

describe Barracks do
  subject :barracks do
    Barracks.new
  end

  describe "#upgrade" do

    it "increases the health of the barracks" do
      barracks.upgrade
      expect(barracks.health).to eq 600
    end

    it 'costs 200 gold' do
      barracks.upgrade
      expect(barracks.gold).to eq 800
    end

    it 'costs 5 food' do
      barracks.upgrade
      expect(barracks.food).to eq 75
    end

    it 'fails if not enough gold' do
      expect(barracks).to receive(:gold).and_return(190)
      expect(barracks.upgrade).to eql false
    end
  end

  describe "#damage" do
    it "takes damage from enemy" do
      footman = Unit.new 300, 5
      footman.attack! barracks
      expect(barracks.health).to eq 395
    end
  end
end


describe Footman do
  subject(:footman) { Footman.new }
  let(:barracks) { Barracks.new }
  describe "#attack!" do
    it 'only does half damage to barracks' do
      old_hp = barracks.health
      footman.attack! barracks
      expect(barracks.health).to eq(old_hp - 5)
    end
  end
end
