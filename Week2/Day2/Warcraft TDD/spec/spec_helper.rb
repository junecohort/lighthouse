# Helper file to setup our tests/specs for this program.
# NO NEED TO EDIT

require 'pry'

def safely_require(file)
  require_relative file
rescue LoadError
  # ignore
end

safely_require '../unit'
safely_require '../footman'
safely_require '../barracks'
#forgot to add
safely_require '../peasant'
safely_require '../seige_engine'
