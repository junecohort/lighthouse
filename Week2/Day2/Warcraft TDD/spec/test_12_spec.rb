require_relative './spec_helper.rb'

describe Unit do
  subject(:unit) { Unit.new(10, 5) }

  describe "#dead?" do
    it "returns true if unit is dead" do
      expect(unit).to receive(:health).and_return(0)
      expect(unit.dead?).to eq true
    end
  end

  describe "#attack!" do
    it "doesn't attack dead units" do
      footman = Unit.new(0, 100000000)
    end

    it "doesn't attack if unit is dead" do
      barracks = Barracks.new
      expect(barracks).to receive(:health).and_return(0)
      expect(unit.attack! barracks).to be_nil
    end
  end
end
