class Primate < Mammal
  attr_reader :stance

  UP_RIGHT = :upright
  DOWNWARD_STANCE = :downward

  def initialize hair_type, stance
    super hair_type, 2
    @stance = stance
  end
end
