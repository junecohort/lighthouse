class Animal
  attr_reader :blood
  WARM_BLOODED = :warm
  COLD_BLOODED = :cold


  def initialize bloodedness
    @blood = bloodedness
  end

  def warm_blooded?
    self.blood == WARM_BLOODED
  end
end
