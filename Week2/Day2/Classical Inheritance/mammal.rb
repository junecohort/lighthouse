class Mammal < Animal
  attr_reader :hair_type, :num_legs
  
  def initialize hair_type, num_legs
    super WARM_BLOODED
    @hair_type = hair_type
    @num_legs = num_legs
  end

  def warm_blooded?
    true
  end
end
