class Chimpanzee < Primate
  attr_reader :name

  include 'super_powers.rb'

  def initialize name
    super 'Brown', UPRIGHT_STANCE
    @name = name
  end
end
