require 'colorize'

def rainbow_string string
  rainbow = string.chars.map! do |x|
    x.colorize([
      :red,
      :green,
      :yellow,
      :magenta,
      :cyan,
      :light_red,
      :light_green,
      :light_yellow,
      :light_blue,
      :light_magenta,
      :light_cyan
    ].sample)
  end
  rainbow.join ""
end
