Answers
===

### 1

`a`: Create, Read, Update, Delete

`b`: Yes with CASCADE

### 2

`a`: The index of the table

`b`: Integers

`c`: It should be unique

### 3

`a`: It joins one table onto another by use of matching keys, it needs a matching key in another table

`b`: An inner join

`c`: Inner joins and displays by similar attribute, outter joins will
produce complete table if no match null.
