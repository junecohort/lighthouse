## 1

    select isbn from editions
    inner join publishers
    on publishers.id = editions.publisher_id
    where publishers.name = 'Random House';

## 2

    select isbn, title from editions
    inner join publishers
    on publishers.id = editions.publisher_id
    inner join books
    on books.id = editions.book_id
    where publishers.name = 'Random House';

## 3

    select editions.isbn, title, stock, retail from editions
    inner join publishers
    on publishers.id = editions.publisher_id
    inner join books
    on books.id = editions.book_id
    inner join stock
    on editions.isbn = stock.isbn
    where publishers.name = 'Random House';

## 4

    select editions.isbn, title, stock, retail from editions
    inner join publishers
    on publishers.id = editions.publisher_id
    inner join books
    on books.id = editions.book_id
    inner join stock
    on editions.isbn = stock.isbn and stock.stock > 0
    where publishers.name = 'Random House';

## 5

    select editions.isbn, title, stock, retail, case
      when editions.type = 'h' then 'Hardcover'
      when editions.type = 'p' then 'Paperback'
    end as type
    from editions
    inner join publishers
    on publishers.id = editions.publisher_id
    inner join books
    on books.id = editions.book_id
    inner join stock
    on editions.isbn = stock.isbn and stock.stock > 0
    where publishers.name = 'Random House';

## 6

    select distinct books.title, editions.publication from books, editions;

## 7

    select sum(stock) from stock;

## 8

    select round(avg(cost),2) as "Average cost",
           round(avg(retail),2) as "Average retail",
           round((avg(retail)-avg(cost)),2) as "Average profit"
    from stock;

## 9

    select editions.book_id, stock.stock from editions
    inner join stock
    on editions.isbn = stock.isbn
    order by stock desc
    limit 1;

## 10

    select a.first_name || ' ' || a.last_name as "Full Name", count(a.id) as "Books Written" from books as b
    inner join editions as e
    on e.book_id = b.id
    inner join authors as a
    on a.id = b.author_id
    group by "Full Name";

## 11

    select concat(concat(a.first_name, ' '), a.last_name) as "Full Name", count(a.id) as "Books Written" from books as b
    inner join editions as e
    on e.book_id = b.id
    inner join authors as a
    on a.id = b.author_id
    group by "Full Name"
    order by "Books Written" desc;

## 12

  select * from books where books.id in (
    select b.book_id
    from (
      select book_id from editions where type = 'h'
    ) b
    inner join
    (
      select book_id from editions where type = 'p'
    ) a
    on b.book_id = a.book_id
  );

## 13

SELECT p.name, ROUND(AVG(s.retail), 2) AS "Average Book Sale Price", COUNT(e.edition) AS "Number of Editions Published"
FROM Publishers AS p
INNER JOIN Editions AS e ON p.id = e.publisher_id
INNER JOIN Stock AS s ON s.isbn = e.isbn
GROUP BY p.name;
