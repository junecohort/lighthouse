require 'bundler/setup'
require 'pry'
require 'open-uri'
require 'active_record'
require 'nokogiri'
require 'sqlite3'
require 'colorize'
require_relative 'lib/light_brow/browser'
require_relative 'lib/light_brow/html_page'

ActiveRecord::Base.establish_connection(
  :adapter => 'sqlite3',
  :database => "db/history.sqlite3"
)

require_relative 'app/models/history'
