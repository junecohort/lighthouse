class AddHistory < ActiveRecord::Migration
  def change
    create_table :histories do |f|
      f.string :url, null: false
    end
  end
end
