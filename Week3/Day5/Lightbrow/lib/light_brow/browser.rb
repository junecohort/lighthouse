module LightBrow
  class Browser

    def run
      welcome
      loop do
        input
        break if quit?
        handle
      end
      History.destroy_all
    end

    private

    def welcome
      menu = <<-menutext
Welcome to Lighthouse Browser - Aka LightBrow
      menutext
      puts menu.colorize(:yellow)
      help
    end

    def help
      help = <<-helptext
COMMANDS:
\\? - help text
\\q - quit
\\h - history
\\b - back
      helptext
      puts help.colorize(:green)
    end

    def handle
      if command?
        handle_command
      else
        visit(@input)
      end
    end

    def command?
      @input.start_with? '\\'
    end

    def handle_command
      case @input
      when '\\?' # \?
        help
      when '\\h' # \h
        history
      when '\\b' # \b
        back
      when '\\f' # \f
        forward
      end
    end

    def history
      counter = 1
      clear_screen
      for history in History.all
        string = "#{counter}: ".red
        string << history.to_s.green
        counter += 1
        puts string
      end
    end

    def clear_screen
      system("clear") || system("cls")
    end

    def back
      clear_screen
      if @current_history == History.first
        visit @current_page.url
      else
        visit History.find(@current_page.id - 1).url
      end
    end

    def forward
      clear_screen
      if @current_history == History.last
        visit @current_page.url
      else
        visit History.find(@current_page.id + 1).url
      end
    end

    def visit url
      if response = fetch(url)
        @page = HTMLPage.new(response)
        display
      else
        puts " ! Invalid URL ! ".black.on_red
      end
    end

    def fetch(url)
      uri = Nokogiri::HTML(open(url).read)
      @current_page = History.find_by_url(url) || History.create(url: url)
      uri
    rescue Errno::ENOENT
    rescue RuntimeError
    end

    def display
      print_info "Title", @page.title
      print_info "Description", @page.description
      print_info "Links"
      @page.links.each_with_index do |link, i|
        puts "#{i}. #{link}"
      end
    end

    def menu
      puts "--".colorize(:light_green)
      puts "Where to next?"
      print "url> ".colorize(:light_green)
    end

    def input
      menu
      @input = gets.chomp
    end

    def quit?
      @input == '\q'
    end

    def print_info(label, value=nil)
      print "#{label}: ".colorize(:red)
      puts value
    end

  end

end
